import { useState } from 'react'
import './App.css'
import MainMenu from './ui/organisms/MainMenu.jsx'

const App = () => {
  const [count, setCount] = useState(0)

  return (
    <main>
      <MainMenu />
    </main>
  )
}

export default App
