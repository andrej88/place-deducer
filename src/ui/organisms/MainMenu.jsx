import React from "react"
import logo from "../../logo.svg"
import "./MainMenu.css"

const MainMenu = () => {
    return (
        <div className="main-menu">
            <img src={logo} className="logo" />
            <div className="new-game-options">
                Welcome to PlaceDeducer, the game where you deduce what place you are at!
            </div>
            <div className="presets">
                Favorite game modes:
                <ul>
                    <li>None so far :(</li>
                </ul>
            </div>
        </div>
    )
}

export default MainMenu
